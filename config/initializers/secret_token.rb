# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
RailsBlog::Application.config.secret_key_base = '5847e2af817acbb4bea94a306c19dfd8bec3dfe9a84855e78b887c46cf8d3446781de1f0eea6895eb38add2f279159f5886858873bf72084af516ab75033f77c'
